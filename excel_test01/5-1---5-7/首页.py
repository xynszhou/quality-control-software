import xlrd
import xlwt

# 打开文件
xls = xlrd.open_workbook('5.1-5.7首页未提交（5.10查询）.xls')

# 选择相应的sheet
sheet = xls.sheet_by_index(0)

# 统计医生姓名及数量
all_data = []
doctor_set = set()

for row_i in range(1, sheet.nrows):
    dep = sheet.cell_value(row_i, 3)
    name = sheet.cell_value(row_i, 1)

    doctor = {
        'name': name,
        'dep': dep,
    }
    all_data.append(doctor)
    print(name + "---" + dep)

for name in all_data:
    doctor_set.add(name['name'])

num_doctor = {}
for name in all_data:
    if name['name'] in doctor_set:
        num_doctor[name['name']] = 0

for name in all_data:
    if name['name'] in doctor_set:
        num_doctor[name['name']] += 1
print("-------------------")
# print(num_doctor)


for name, num in num_doctor.items():
    print('{name}:{num},罚款{fee}元'.format(name=name, num=num, fee=num * 30))
# print(sheet.nrows - 1)
# print(all_data)
# print(doctor_set)

# 新建excel workbook
new_workbook = xlwt.Workbook()
worksheet = new_workbook.add_sheet('罚款统计')
worksheet.write(0, 0, '姓名')
worksheet.write(0, 1, '未完成数（个）')
worksheet.write(0, 2, '罚款金额（元）')

row = 0
number = 0
for doc, num in num_doctor.items():
    worksheet.write(row + 1, 0, doc)
    worksheet.write(row + 1, 1, num)
    worksheet.write(row + 1, 2, num * 30)
    row += 1
    number += num

print(number)

new_workbook.save('首页罚款表.xls')
